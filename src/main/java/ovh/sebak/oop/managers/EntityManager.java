package ovh.sebak.oop.managers;

import ovh.sebak.oop.objects.Order;
import ovh.sebak.oop.objects.base.Animal;
import ovh.sebak.oop.objects.base.Entity;
import ovh.sebak.oop.objects.enums.EntityType;

import java.util.ArrayList;

public class EntityManager {

    private static int idGenerator = 1;

    private static ArrayList<Entity> entities = new ArrayList<>();
    private static ArrayList<Order> orders = new ArrayList<>();


    public static ArrayList<Entity> getEntities() {
        return entities;
    }

    public static ArrayList<Animal> getAvailableAnimals() {
        ArrayList<Animal> list = new ArrayList<>();

        for (Entity entity : entities) {
            if (!(entity instanceof Animal)) {
                continue;
            }

            Animal animal = (Animal) entity;
            boolean isAvailable = true;
            for (Order order : orders) {
                if (order.getAnimals().contains(animal)) {
                    isAvailable = false;
                    break;
                }
            }

            if (!isAvailable) {
                continue;
            }

            list.add(animal);
        }

        return list;
    }

    public static ArrayList<Entity> getEntitiesByType(EntityType type) {
        ArrayList<Entity> list = new ArrayList<>();

        for (Entity entity : entities) {
            if (entity.getType() != type) {
                continue;
            }

            list.add(entity);
        }

        return list;
    }

    public static ArrayList<Entity> getEntitiesByName(String name) {
        ArrayList<Entity> list = new ArrayList<>();

        for (Entity entity : entities) {
            if (!entity.getName().contains(name)) {
                continue;
            }

            list.add(entity);
        }

        return list;
    }

    public static Entity getEntitiesById(int id) {
        for (Entity entity : entities) {
            if (entity.getId() == id) {
                return entity;
            }

        }

        return null;
    }

    public static void addEntity(Entity entity) {
        if (entities.contains(entity)) {
            return;
        }

        entities.add(entity);
    }

    public static void removeEntity(Entity entity) {
        if (!entities.contains(entity)) {
            return;
        }

        entities.remove(entity);
    }

    public static void removeEntityById(int id) {
        Entity toRemove = null;
        for (Entity entity : entities) {
            if (entity.getId() != id) {
                continue;
            }

            toRemove = entity;
            break;
        }

        if (toRemove != null) {
            entities.remove(toRemove);
        }
    }

    public static int generateId() {
        return idGenerator++;
    }

    public static ArrayList<Order> getOrders() {
        return orders;
    }

    public static void addOrder(Order order) {
        if (orders.contains(order)) {
            return;
        }

        orders.add(order);
    }
}
