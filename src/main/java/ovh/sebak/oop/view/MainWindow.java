package ovh.sebak.oop.view;

import ovh.sebak.oop.managers.EntityManager;
import ovh.sebak.oop.objects.base.Entity;
import ovh.sebak.oop.objects.interfaces.Earner;
import ovh.sebak.oop.view.panels.*;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class MainWindow extends JFrame {

    private JPanel panelMain;
    private JPanel panelContent;

    public MainWindow() {
        initialize();
    }

    private void initialize() {
        final MainWindow instance = this;

        setTitle("Aplikacja");
        setResizable(false);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 900, 600);


        panelMain = new JPanel();
        panelMain.setBorder(new EmptyBorder(5, 5, 5, 5));
        panelMain.setLayout(null);
        setContentPane(panelMain);


        JButton btnAdd = new JButton("Dodaj obiekt");
        btnAdd.setBounds(25, 13, 180, 40);
        btnAdd.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                changePanel(new CreatePanel());
            }
        });
        panelMain.add(btnAdd);

        JButton btnDelete = new JButton("Usuń obiekt");
        btnDelete.setBounds(25, 66, 180, 40);
        btnDelete.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                changePanel(new DeletePanel());
            }
        });
        panelMain.add(btnDelete);

        JButton btnShowAll = new JButton("Poka\u017C obiekty");
        btnShowAll.setBounds(25, 119, 180, 40);
        btnShowAll.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                changePanel(new ListPanel());
            }
        });
        panelMain.add(btnShowAll);

        JButton btnShowByType = new JButton("Poka\u017C obiekty wg. typu");
        btnShowByType.setBounds(25, 172, 180, 40);
        btnShowByType.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                changePanel(new SearchByTypePanel());
            }
        });
        panelMain.add(btnShowByType);

        JButton btnShowByName = new JButton("Poka\u017C obiekty wg. nazwy");
        btnShowByName.setBounds(25, 225, 180, 40);
        btnShowByName.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                changePanel(new SearchByNamePanel());
            }
        });
        panelMain.add(btnShowByName);

        JButton btnActionAll = new JButton("Daj podwyżke");
        btnActionAll.setBounds(25, 278, 180, 40);
        btnActionAll.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                giveRaise();
                JOptionPane.showMessageDialog(null,
                        "Dodano podwyżkę każdemu pracownikowi i liderowi.",
                        "",
                        JOptionPane.INFORMATION_MESSAGE);
            }
        });
        panelMain.add(btnActionAll);

        JButton btnOrderCreate = new JButton("Dodaj zamówienie");
        btnOrderCreate.setBounds(25, 328, 180, 40);
        btnOrderCreate.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                changePanel(new OrderCreatePanel(instance));
            }
        });
        panelMain.add(btnOrderCreate);

        JButton btnOrderList = new JButton("Lista zamówień");
        btnOrderList.setBounds(25, 378, 180, 40);
        btnOrderList.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                changePanel(new OrderListPanel());
            }
        });
        panelMain.add(btnOrderList);
    }

    public void changePanel(JPanel newPanel) {
        if (this.panelContent != null) {
            panelMain.remove(this.panelContent);
            this.panelContent = null;
        }

        if (newPanel == null) {
            repaint();
            return;
        }

        newPanel.setBounds(250, 13, 601, 527);
        newPanel.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));

        this.panelContent = newPanel;
        panelMain.add(newPanel);

        repaint();

        if (newPanel instanceof ListPanel) {
            ListPanel listPanel = (ListPanel) newPanel;
            listPanel.postInit();
        }

        if (newPanel instanceof OrderCreatePanel) {
            OrderCreatePanel orderCreatePanel = (OrderCreatePanel) newPanel;
            orderCreatePanel.postInit();
        }

        if (newPanel instanceof OrderListPanel) {
            OrderListPanel orderListPanel = (OrderListPanel) newPanel;
            orderListPanel.postInit();
        }
    }

    private void giveRaise() {
        for (Entity entity : EntityManager.getEntities()) {
            if (!(entity instanceof Earner)) {
                continue;
            }

            Earner earner = (Earner) entity;
            earner.giveRaise(500);
        }
    }

}
