package ovh.sebak.oop.view.panels;

import ovh.sebak.oop.managers.EntityManager;
import ovh.sebak.oop.objects.base.Entity;
import ovh.sebak.oop.objects.enums.EntityType;

import javax.swing.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class SearchByTypePanel extends JPanel {

	private JComboBox selectType;

	private DefaultListModel<String> listModel;

	public SearchByTypePanel() {
		setLayout(null);

		JLabel labelHint = new JLabel("Wybierz szukany typ:");
		labelHint.setBounds(15, 15, 159, 25);
		add(labelHint);
		
		selectType = new JComboBox();
		selectType.setModel(new DefaultComboBoxModel(EntityType.values()));
		selectType.setBounds(15, 40, 450, 30);
		add(selectType);
		
		JButton btnSearchSubmit = new JButton("Szukaj");
		btnSearchSubmit.setBounds(463, 40, 123, 30);
		btnSearchSubmit.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				searchAndLoadList();
			}
		});
		add(btnSearchSubmit);

		listModel = new DefaultListModel<>();

		JList<String> list = new JList<>(listModel);

		JScrollPane scrollPane = new JScrollPane(list);
		scrollPane.setBounds(10,80,581, 437);
		add(scrollPane);
	}

	private void searchAndLoadList() {
		EntityType type = (EntityType) this.selectType.getSelectedItem();

		listModel.clear();

		for (Entity entity : EntityManager.getEntitiesByType(type)) {
			listModel.addElement(
					String.format("<html>%s<br>&nbsp;</html>", entity.renderToList())
			);
		}

		revalidate();
	}

}
