package ovh.sebak.oop.view.panels;

import ovh.sebak.oop.managers.EntityManager;
import ovh.sebak.oop.objects.base.Entity;

import javax.swing.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class SearchByNamePanel extends JPanel {

	private JTextField inputName;

	private DefaultListModel<String> listModel;

	public SearchByNamePanel() {
		setLayout(null);

		JLabel labelHint = new JLabel("Podaj szukan\u0105 nazw\u0119:");
		labelHint.setBounds(15, 15, 159, 25);
		add(labelHint);

		inputName = new JTextField();
		inputName.setBounds(15, 40, 450, 30);
		add(inputName);
		
		JButton btnSearchSubmit = new JButton("Szukaj");
		btnSearchSubmit.setBounds(463, 40, 123, 29);
		btnSearchSubmit.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				searchAndLoadList();
			}
		});
		add(btnSearchSubmit);

		listModel = new DefaultListModel<>();

		JList<String> list = new JList<>(listModel);

		JScrollPane scrollPane = new JScrollPane(list);
		scrollPane.setBounds(10,80,581, 437);
		add(scrollPane);


	}

	private void searchAndLoadList() {
		String name = this.inputName.getText();

		listModel.clear();

		for (Entity entity : EntityManager.getEntitiesByName(name)) {
			listModel.addElement(
					String.format("<html>%s<br>&nbsp;</html>", entity.renderToList())
			);
		}

		revalidate();
	}

}
