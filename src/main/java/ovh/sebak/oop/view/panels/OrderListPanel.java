package ovh.sebak.oop.view.panels;

import ovh.sebak.oop.managers.EntityManager;
import ovh.sebak.oop.objects.Order;
import ovh.sebak.oop.objects.base.Entity;

import javax.swing.*;

public class OrderListPanel extends JPanel {

    public OrderListPanel() {
        setLayout(null);

        DefaultListModel<String> listModel = new DefaultListModel<>();

        JList<String> list = new JList<>(listModel);

        JScrollPane scrollPane = new JScrollPane(list);
        scrollPane.setBounds(10,10,581, 507);
        add(scrollPane);


        for (Order order : EntityManager.getOrders()) {
            listModel.addElement(
                    String.format("<html>%s<br>&nbsp;</html>", order.renderToList())
            );
        }

    }

    public void postInit() {
        revalidate();
    }
}
