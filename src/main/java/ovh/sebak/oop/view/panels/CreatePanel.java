package ovh.sebak.oop.view.panels;

import ovh.sebak.oop.managers.EntityManager;
import ovh.sebak.oop.objects.*;
import ovh.sebak.oop.objects.enums.EntityType;
import ovh.sebak.oop.view.MainWindow;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class CreatePanel extends JPanel {

    private MainWindow mainWindow;

    private JComboBox<EntityType> selectType;

    private JTextField input1;
    private JTextField input2;
    private JTextField input3;
    private JTextField input4;
    private JTextField input5;

    private JLabel label1;
    private JLabel label2;
    private JLabel label3;
    private JLabel label4;
    private JLabel label5;

    public CreatePanel() {
        setLayout(null);

        JLabel labelHint = new JLabel("Wybierz dodawany typ:");
        labelHint.setBounds(15, 15, 159, 25);
        add(labelHint);

        selectType = new JComboBox<>();
        selectType.setModel(new DefaultComboBoxModel<>(EntityType.values()));
        selectType.setBounds(15, 40, 450, 30);
        add(selectType);

        JButton btnSearchSubmit = new JButton("Wybierz");
        btnSearchSubmit.setBounds(463, 40, 123, 30);
        btnSearchSubmit.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                showForm();
            }
        });
        add(btnSearchSubmit);


        JButton btnAddSubmit = new JButton("Dodaj");
        btnAddSubmit.setFont(new Font("Tahoma", Font.PLAIN, 15));
        btnAddSubmit.setBounds(442, 472, 147, 42);
        btnAddSubmit.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                submitForm();
            }
        });
        add(btnAddSubmit);

        label1 = new JLabel("LABEL 1");
        label1.setBounds(12, 88, 200, 16);
        add(label1);

        input1 = new JTextField();
        input1.setColumns(10);
        input1.setBounds(12, 113, 200, 22);
        add(input1);

        label2 = new JLabel("LABEL 2");
        label2.setBounds(12, 159, 200, 16);
        add(label2);

        input2 = new JTextField();
        input2.setColumns(10);
        input2.setBounds(12, 184, 200, 22);
        add(input2);

        label3 = new JLabel("LABEL 3");
        label3.setBounds(12, 238, 200, 16);
        add(label3);

        input3 = new JTextField();
        input3.setColumns(10);
        input3.setBounds(12, 263, 200, 22);
        add(input3);

        label4 = new JLabel("LABEL 4");
        label4.setBounds(12, 326, 200, 16);
        add(label4);

        input4 = new JTextField();
        input4.setColumns(10);
        input4.setBounds(12, 351, 200, 22);
        add(input4);

        label5 = new JLabel("LABEL 5");
        label5.setBounds(327, 88, 200, 16);
        add(label5);

        input5 = new JTextField();
        input5.setColumns(10);
        input5.setBounds(327, 113, 200, 22);
        add(input5);

        resetForm();
    }

    private void resetForm() {
        label1.setVisible(false);
        input1.setVisible(false);
        input1.setText("");

        label2.setVisible(false);
        input2.setVisible(false);
        input2.setText("");

        label3.setVisible(false);
        input3.setVisible(false);
        input3.setText("");

        label4.setVisible(false);
        input4.setVisible(false);
        input4.setText("");

        label5.setVisible(false);
        input5.setVisible(false);
        input5.setText("");
    }

    private void showForm() {
        resetForm();

        EntityType type = (EntityType) this.selectType.getSelectedItem();

        if (type == null) {
            return;
        }

        showField(label1, input1, "Nazwa");

        if (type == EntityType.CAT || type == EntityType.DOG) {
            showField(label2, input2, "Kolor");
            showField(label3, input3, "Cena");
        }

        if (type == EntityType.CLIENT || type == EntityType.EMPLOYEE || type == EntityType.LEADER) {
            showField(label2, input2, "Pesel");
        }

        if (type == EntityType.EMPLOYEE || type == EntityType.LEADER) {
            showField(label3, input3, "Wynagrodzenie");
        }

        switch (type) {
            case CAT:
                showField(label4, input4, "Pochodzenie");
                break;
            case DOG:
                showField(label4, input4, "Rasa");
                break;
            case CLIENT:
                showField(label3, input3, "Numer stałego klienta");
                break;
            case EMPLOYEE:
                showField(label4, input4, "Oddział");
                break;
            case LEADER:
                showField(label4, input4, "Miasto");
                break;
        }
    }

    private void showField(JLabel label, JTextField input, String fieldName) {
        label.setText(fieldName);

        label.setVisible(true);
        input.setVisible(true);
    }

    private void submitForm() {
        EntityType type = (EntityType) this.selectType.getSelectedItem();

        if (type == null) {
            return;
        }

        boolean success = false;

        switch (type) {
            case CAT:
                success = this.formAddCat();
                break;
            case DOG:
                success = this.formAddDog();
                break;
            case CLIENT:
                success = this.formAddClient();
                break;
            case EMPLOYEE:
                success = this.formAddEmployee();
                break;
            case LEADER:
                success = this.formAddLeader();
                break;
        }

        if (success) {
            JOptionPane.showMessageDialog(null, "Obiekt został dodany.", "Sukces", JOptionPane.INFORMATION_MESSAGE);
            this.resetForm();
        }
    }

    private boolean formAddCat() {
        String name = this.input1.getText();
        double price;
        String color = this.input2.getText();
        String origin = this.input3.getText();

        try {
            price = Double.parseDouble(this.input3.getText());
        } catch (NumberFormatException e) {

            JOptionPane.showMessageDialog(null, "Wprowadzono błędną cenę.", "Błąd", JOptionPane.ERROR_MESSAGE);
            return false;
        }

        Cat cat = new Cat(
                EntityManager.generateId(),
                name,
                price,
                color,
                origin
        );
        EntityManager.addEntity(cat);

        return true;
    }

    private boolean formAddDog() {
        String name = this.input1.getText();
        double price;
        String color = this.input2.getText();
        String breed = this.input3.getText();

        try {
            price = Double.parseDouble(this.input3.getText());
        } catch (NumberFormatException e) {

            JOptionPane.showMessageDialog(null, "Wprowadzono błędną cenę.", "Błąd", JOptionPane.ERROR_MESSAGE);
            return false;
        }

        Dog dog = new Dog(
                EntityManager.generateId(),
                name,
                price,
                color,
                breed
        );
        dog.setColor(color);
        EntityManager.addEntity(dog);

        return true;
    }

    private boolean formAddClient() {
        String name = this.input1.getText();
        String pesel = this.input2.getText();
        String regularCustomerNumber = this.input3.getText();

        Client client = new Client(
                EntityManager.generateId(),
                name,
                pesel,
                regularCustomerNumber
        );
        EntityManager.addEntity(client);

        return true;
    }

    private boolean formAddEmployee() {
        String name = this.input1.getText();
        String pesel = this.input2.getText();
        int salary;
        String department = this.input4.getText();

        try {
            salary = Integer.parseInt(this.input3.getText());
        } catch (NumberFormatException e) {

            JOptionPane.showMessageDialog(null, "Wprowadzono błędne wynagrodzenie.", "Błąd", JOptionPane.ERROR_MESSAGE);
            return false;
        }

        Employee employee = new Employee(
                EntityManager.generateId(),
                name,
                pesel,
                salary,
                department
        );
        EntityManager.addEntity(employee);

        return true;
    }

    private boolean formAddLeader() {
        String name = this.input1.getText();
        String pesel = this.input2.getText();
        int salary;
        String city = this.input4.getText();

        try {
            salary = Integer.parseInt(this.input3.getText());
        } catch (NumberFormatException e) {

            JOptionPane.showMessageDialog(null, "Wprowadzono błędne wynagrodzenie.", "Błąd", JOptionPane.ERROR_MESSAGE);
            return false;
        }

        Leader leader = new Leader(
                EntityManager.generateId(),
                name,
                pesel,
                salary,
                city
        );
        EntityManager.addEntity(leader);

        return true;
    }


}
