package ovh.sebak.oop.view.panels;

import ovh.sebak.oop.managers.EntityManager;
import ovh.sebak.oop.objects.Client;
import ovh.sebak.oop.objects.Order;
import ovh.sebak.oop.objects.base.Animal;
import ovh.sebak.oop.objects.base.Entity;
import ovh.sebak.oop.view.MainWindow;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

public class OrderCreatePanel extends JPanel {

    private MainWindow mainWindow;

    private JTextField inputClient;

    private JList list;

    public OrderCreatePanel(MainWindow mainWindow) {
        this.mainWindow = mainWindow;
        setLayout(null);

        JLabel labelHint = new JLabel("Podaj id klienta:");
        labelHint.setBounds(15, 15, 159, 25);
        add(labelHint);

        inputClient = new JTextField();
        inputClient.setBounds(15, 40, 450, 30);
        add(inputClient);

        DefaultListModel listModel = new DefaultListModel();

        list = new JList(listModel);

        JScrollPane scrollPane = new JScrollPane(list);
        scrollPane.setBounds(10,80,581, 367);
        add(scrollPane);


        for (Animal animal : EntityManager.getAvailableAnimals()) {
            listModel.addElement(animal);
        }

        JButton btnAddSubmit = new JButton("Dodaj");
        btnAddSubmit.setFont(new Font("Tahoma", Font.PLAIN, 15));
        btnAddSubmit.setBounds(442, 472, 147, 42);
        btnAddSubmit.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                submitForm();
            }
        });
        add(btnAddSubmit);

    }

    public void postInit() {
        revalidate();
    }

    private void submitForm() {
        int id = 0;

        try {
            id = Integer.parseInt(this.inputClient.getText());
        } catch (NumberFormatException e) {
            this.inputClient.setText("");

            JOptionPane.showMessageDialog(null,
                    "Wprowadzono błędny numer ID.",
                    "Błąd",
                    JOptionPane.ERROR_MESSAGE);
            return;
        }

        if (id == 0) {
            return;
        }

        Entity entity = EntityManager.getEntitiesById(id);
        if (!(entity instanceof Client)) {
            JOptionPane.showMessageDialog(null,
                    "Nie znaleziono klienta.",
                    "Błąd",
                    JOptionPane.ERROR_MESSAGE);
            return;
        }

        Client client = (Client) entity;
        List<Animal> animals = new ArrayList<>();

        for (Object o : list.getSelectedValuesList()) {
            if (!(o instanceof Animal)) {
                continue;
            }
            Animal animal = (Animal) o;

            animals.add(animal);
        }

        if (animals.isEmpty()) {
            JOptionPane.showMessageDialog(null,
                    "Dodaj zwierzę do zamówienia.",
                    "Błąd",
                    JOptionPane.ERROR_MESSAGE);
            return;
        }

        Order order = new Order(client, animals);
        EntityManager.addOrder(order);

        JOptionPane.showMessageDialog(null, "Zamówienie zostało stworzone", "Sukces", JOptionPane.INFORMATION_MESSAGE);

        this.mainWindow.changePanel(new OrderListPanel());
    }

}
