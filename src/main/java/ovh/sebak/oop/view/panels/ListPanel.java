package ovh.sebak.oop.view.panels;

import ovh.sebak.oop.managers.EntityManager;
import ovh.sebak.oop.objects.base.Entity;

import javax.swing.*;

public class ListPanel extends JPanel {

    public ListPanel() {
        setLayout(null);

        DefaultListModel<String> listModel = new DefaultListModel<>();

        JList<String> list = new JList<>(listModel);

        JScrollPane scrollPane = new JScrollPane(list);
        scrollPane.setBounds(10,10,581, 507);
        add(scrollPane);


        for (Entity entity : EntityManager.getEntities()) {
            listModel.addElement(
                    String.format("<html>%s<br>&nbsp;</html>", entity.renderToList())
            );
        }

    }

    public void postInit() {
        revalidate();
    }
}
