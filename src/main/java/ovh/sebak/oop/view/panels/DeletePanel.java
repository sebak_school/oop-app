package ovh.sebak.oop.view.panels;

import ovh.sebak.oop.managers.EntityManager;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class DeletePanel extends JPanel {

    private JTextField inputId;

    public DeletePanel() {
        setLayout(null);

        JButton btnRemoveSubmit = new JButton("Usuń");
        btnRemoveSubmit.setFont(new Font("Tahoma", Font.PLAIN, 15));
        btnRemoveSubmit.setBounds(442, 472, 147, 42);
        btnRemoveSubmit.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                removeEntity();
            }
        });
        add(btnRemoveSubmit);

        JLabel lblNewLabel = new JLabel("Podaj ID:");
        lblNewLabel.setBounds(12, 13, 200, 16);
        add(lblNewLabel);

        inputId = new JTextField();
        inputId.setBounds(12, 38, 200, 22);
        add(inputId);
    }

    public void removeEntity() {
        int id = 0;

        try {
            id = Integer.parseInt(this.inputId.getText());
        } catch (NumberFormatException e) {
            this.inputId.setText("");
            
            JOptionPane.showMessageDialog(null,
                    "Wprowadzono błędny numer ID.",
                    "Błąd",
                    JOptionPane.ERROR_MESSAGE);
            return;
        }

        if (id == 0) {
            return;
        }

        this.inputId.setText("");

        EntityManager.removeEntityById(id);

        JOptionPane.showMessageDialog(null,
                "Obiekt został usunięty.",
                "Sukces",
                JOptionPane.INFORMATION_MESSAGE);
    }

}
