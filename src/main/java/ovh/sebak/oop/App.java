package ovh.sebak.oop;

import ovh.sebak.oop.managers.EntityManager;
import ovh.sebak.oop.objects.*;
import ovh.sebak.oop.view.MainWindow;

public class App {

    private MainWindow mainWindow;

    public void start() {

        mainWindow = new MainWindow();
        mainWindow.setVisible(true);

        testData();
    }

    private void testData() {

        Cat cat1 = new Cat(
                EntityManager.generateId(),
                "Pat",
                100.00,
                "Czarny",
                "Egipt"
        );
        EntityManager.addEntity(cat1);

        Cat cat2 = new Cat(
                EntityManager.generateId(),
                "Mat",
                25.99,
                "Biały",
                "Indie"
        );
        EntityManager.addEntity(cat2);

        Dog dog1 = new Dog(
                EntityManager.generateId(),
                "Azor",
                49.99,
                "Brązowy",
                "Owczarek"
        );
        EntityManager.addEntity(dog1);

        Client client1 = new Client(
                EntityManager.generateId(),
                "Jan",
                "12345678900",
                "A 00/11"
        );
        EntityManager.addEntity(client1);

        Client client2 = new Client(
                EntityManager.generateId(),
                "Piotr",
                "00987654321",
                "B 00/123123"
        );
        EntityManager.addEntity(client2);

        Employee employee1 = new Employee(
                EntityManager.generateId(),
                "Mateusz",
                "01122334455",
                3500,
                "HR"
        );
        EntityManager.addEntity(employee1);

        Employee employee2 = new Employee(
                EntityManager.generateId(),
                "Artur",
                "66554433221",
                4000,
                "Finanse"
        );
        EntityManager.addEntity(employee2);

        Leader leader1 = new Leader(
                EntityManager.generateId(),
                "Hubert",
                "00000000000",
                5000,
                "Poznań"
        );
        EntityManager.addEntity(leader1);

    }

}
