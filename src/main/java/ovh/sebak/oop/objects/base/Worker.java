package ovh.sebak.oop.objects.base;

public abstract class Worker extends Person {

    protected int salary;

    public Worker(int id, String name, String pesel, int salary) {
        super(id, name, pesel);

        this.salary = salary;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

}
