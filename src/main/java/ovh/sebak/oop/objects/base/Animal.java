package ovh.sebak.oop.objects.base;

public abstract class Animal extends Entity {

    private double price;
    private String color;

    public Animal(int id, String name, double price, String color) {
        super(id, name);

        this.price = price;
        this.color = color;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String toString() {
        return this.renderToList();
    }

}
