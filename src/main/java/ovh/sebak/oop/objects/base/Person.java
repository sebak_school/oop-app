package ovh.sebak.oop.objects.base;

public abstract class Person extends Entity {

    private String pesel;

    public Person(int id, String name, String pesel) {
        super(id, name);

        this.pesel = pesel;
    }

    public String getPesel() {
        return this.pesel;
    }

    public void setPesel(String pesel) {
        this.pesel = pesel;
    }
}
