package ovh.sebak.oop.objects.base;

import ovh.sebak.oop.objects.enums.EntityType;

import java.text.SimpleDateFormat;
import java.util.Date;

public abstract class Entity {

    private int id;
    private String name;
    private long createdAt;

    public Entity(int id, String name) {
        this.id = id;
        this.name = name;
        this.createdAt = System.currentTimeMillis();
    }

    public abstract EntityType getType();

    public abstract String renderToList();

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getCreatedAt() {
        return createdAt;
    }

    public String getCreatedAtAsString() {
        return new SimpleDateFormat("yyyy-MM-dd HH:mm").format(new Date(this.createdAt));
    }

}
