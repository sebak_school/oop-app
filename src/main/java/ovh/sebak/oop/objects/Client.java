package ovh.sebak.oop.objects;

import ovh.sebak.oop.objects.base.Person;
import ovh.sebak.oop.objects.enums.EntityType;

public class Client extends Person {

    private String regularCustomerNumber;

    public Client(int id, String name, String pesel, String regularCustomerNumber) {
        super(id, name, pesel);
        
        this.regularCustomerNumber = regularCustomerNumber;
    }

    public String getRegularCustomerNumber() {
        return regularCustomerNumber;
    }

    public void setRegularCustomerNumber(String regularCustomerNumber) {
        this.regularCustomerNumber = regularCustomerNumber;
    }

    public EntityType getType() {
        return EntityType.CLIENT;
    }

    public String renderToList() {
        return String.format(
                "[%d -> Klient] Imie: %s, Pesel: %s, Numer stałego klienta: %s, Data dodania: %s",
                this.getId(),
                this.getName(),
                this.getPesel(),
                this.getRegularCustomerNumber(),
                this.getCreatedAtAsString()
        );
    }
}
