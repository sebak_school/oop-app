package ovh.sebak.oop.objects;

import ovh.sebak.oop.objects.base.Animal;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Order {

    private Client client;
    private List<Animal> animals;
    private long createdAt;

    public Order(Client client, List<Animal> animals) {
        this.client = client;
        this.animals = animals;

        this.createdAt = System.currentTimeMillis();
    }

    public Client getClient() {
        return client;
    }

    public List<Animal> getAnimals() {
        return animals;
    }

    public long getCreatedAt() {
        return createdAt;
    }

    private double calcPrice() {
        double price = 0;
        for (Animal animal : this.animals) {
            price += animal.getPrice();
        }
        return price;
    }

    public String renderToList() {
        StringBuilder animalsList = new StringBuilder();
        for (Animal animal : this.animals) {
            animalsList.append(String.format("%s (%.2f), ", animal.getName(), animal.getPrice()));
        }
        animalsList.setLength(
                Math.max(0, animalsList.length() - 2)
        );

        return String.format(
                "[Zamówienie] Klient: %s, Koszt: %.2f, Zwierzeta: [%s], Data złożenia: %s",
                this.getClient().getName(),
                this.calcPrice(),
                animalsList.toString(),
                new SimpleDateFormat("yyyy-MM-dd HH:mm").format(new Date(this.getCreatedAt()))
        );
    }
}
