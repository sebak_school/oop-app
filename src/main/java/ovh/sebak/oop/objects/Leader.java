package ovh.sebak.oop.objects;

import ovh.sebak.oop.objects.base.Person;
import ovh.sebak.oop.objects.base.Worker;
import ovh.sebak.oop.objects.enums.EntityType;
import ovh.sebak.oop.objects.interfaces.Earner;

public class Leader extends Worker implements Earner {

    private String city;

    public Leader(int id, String name, String pesel, int salary, String city) {
        super(id, name, pesel, salary);

        this.city = city;
    }

    public void giveRaise(int raise) {
        this.salary += raise;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public EntityType getType() {
        return EntityType.LEADER;
    }

    public String renderToList() {
        return String.format(
                "[%d -> Lider] Imie: %s, Pesel: %s, Wynagrodzenie: %d, Miasto: %s, Data dodania: %s",
                this.getId(),
                this.getName(),
                this.getPesel(),
                this.getSalary(),
                this.getCity(),
                this.getCreatedAtAsString()
        );
    }
}
