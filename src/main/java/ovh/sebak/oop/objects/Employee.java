package ovh.sebak.oop.objects;

import ovh.sebak.oop.objects.base.Person;
import ovh.sebak.oop.objects.base.Worker;
import ovh.sebak.oop.objects.enums.EntityType;
import ovh.sebak.oop.objects.interfaces.Earner;

public class Employee extends Worker implements Earner {

    private String department;

    public Employee(int id, String name, String pesel, int salary, String department) {
        super(id, name, pesel, salary);

        this.department = department;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public void giveRaise(int raise) {
        this.salary += raise;
    }

    public EntityType getType() {
        return EntityType.EMPLOYEE;
    }

    public String renderToList() {
        return String.format(
                "[%d -> Pracownik] Imie: %s, Pesel: %s, Wynagrodzenie: %d, Dział: %s, Data dodania: %s",
                this.getId(),
                this.getName(),
                this.getPesel(),
                this.getSalary(),
                this.getDepartment(),
                this.getCreatedAtAsString()
        );
    }
}
