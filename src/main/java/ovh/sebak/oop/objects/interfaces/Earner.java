package ovh.sebak.oop.objects.interfaces;

public interface Earner {

    void giveRaise(int raise);

}
