package ovh.sebak.oop.objects;

import ovh.sebak.oop.objects.base.Animal;
import ovh.sebak.oop.objects.enums.EntityType;

public class Cat extends Animal {

    private String origin;

    public Cat(int id, String name, double price, String color, String origin) {
        super(id, name, price, color);

        this.origin = origin;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public EntityType getType() {
        return EntityType.CAT;
    }

    public String renderToList() {
        return String.format(
                "[%d -> Kot] Nazwa: %s, Kolor: %s, Pochodzenie: %s, Cena: %.2f, Data dodania: %s",
                this.getId(),
                this.getName(),
                this.getColor(),
                this.getOrigin(),
                this.getPrice(),
                this.getCreatedAtAsString()
        );
    }
}
