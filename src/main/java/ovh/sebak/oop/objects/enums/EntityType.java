package ovh.sebak.oop.objects.enums;

public enum EntityType {
    CAT,
    DOG,

    CLIENT,
    EMPLOYEE,
    LEADER,
}
