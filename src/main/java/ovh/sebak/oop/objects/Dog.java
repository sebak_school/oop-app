package ovh.sebak.oop.objects;

import ovh.sebak.oop.objects.base.Animal;
import ovh.sebak.oop.objects.enums.EntityType;

public class Dog extends Animal {

    public String breed;

    public Dog(int id, String name, double price, String color, String breed) {
        super(id, name, price, color);

        this.breed = breed;
    }

    public String getBreed() {
        return breed;
    }

    public void setBreed(String breed) {
        this.breed = breed;
    }

    public EntityType getType() {
        return EntityType.DOG;
    }

    public String renderToList() {
        return String.format("[%d -> Pies] Nazwa: %s, Kolor: %s, Rasa: %s, Cena: %.2f, Data dodania: %s",
                this.getId(),
                this.getName(),
                this.getColor(),
                this.getBreed(),
                this.getPrice(),
                this.getCreatedAtAsString()
        );
    }

}
